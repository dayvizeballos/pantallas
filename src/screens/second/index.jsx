import { Button, Text, TextInput, View } from 'react-native';
import React, { useState } from 'react';

import { styles } from "./styles";

const SegundaVista = ({navigation, route}) => {
    
    const [item, setItem] = useState('');

    const inputvalue = item.trim();
    
    const onModify = () => {
      navigation.navigate("primera",{
        id : route.params.objeto.id,
        valor : item,
      })
    }

    const onChangeText = (text) => {
      setItem(text)
    }
    
    return (
        <View style={styles.container}>
          <View style={styles.containerGeneral}>

            <View style={styles.inputContainer}>
                <Text style={styles.label}>Modificar categoria</Text>
                <TextInput 
                    item={item}
                    style={styles.input}
                    placeholderTextColor='#cccccc'
                    placeholder='Modificar categoria'
                    onChangeText={onChangeText}
                    defaultValue={route.params.objeto.value}
                    inputValue={inputvalue}
                />
                <View style={styles.button}>
                    <Button title='Modificar' color='#fff' onPress={() => onModify()}/>
                </View>

                <Button title="Regresar a la lista" onPress={() => navigation.navigate("primera")} color="#1493e2" />
            </View>

         </View>
        
        </View>
      );
};

export default SegundaVista;