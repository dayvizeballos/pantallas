import { StyleSheet } from "react-native";
import colors from '../../constants/colors';

export const styles = StyleSheet.create({
    container:{
      flex: 1,
      width: '100%',
    },
    containerGeneral: {
      flex: 1,
      paddingHorizontal:10,
      backgroundColor: '#D4CDFF',
    },
    itemList: {
      flex: 1,
    },
    buttonContainer: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginVertical: 10,
      backgroundColor: colors.confirma,
      borderRadius: 7,
      shadowColor: "#000",
      shadowOffset: {
          width: 0,
          height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: 3,
    },
    modal: {
      flex : 1,
      backgroundColor: 'rgba(1,1,1, 0.5)',
      justifyContent: 'center',
      alignItems: 'center',
    },
    modalContent: {
      height: '35%',
      width: '90%',
      backgroundColor: '#fff',
    },
    modalBox: {
      height: 45,
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
      paddingHorizontal: 15,
    },
    message:{
      color: '#1493e2',
    }
  });