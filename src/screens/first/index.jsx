import { Button, FlatList, Image, Text, TouchableOpacity, View } from 'react-native';
import { CustomInput, CustomModal, ListaItems } from '../../components/index';
import React, { useState } from 'react';

import {LISTA_CLASIFICACION} from "../../constants/clasificacion";
import close  from '../../../assets/cerrar.png';
import { styles } from "./styles";

const PrimeraVista = ({navigation, route}) => {
    
      const [itemList, setItemList] = useState(LISTA_CLASIFICACION);
      const [modalVisible, setModalVisible] = useState(false);
      const [itemSelected, setItemSelected] = useState({});
      const [item, setItem] = useState('');
      const inputvalue = item.trim();
    
      const renderItem = ({ item }) => (
        <ListaItems item={item}  onHandlerModal={onHandlerModal} onSelected={onSelected} onCambio={onCambio}/>
      )
      const onChangeText = (text) => {
        setItem(text)
      }

      const onCambio = (id, text) =>{
       
      }
      
      if(route.params?.id && route.params?.valor){
        itemList.forEach((item) => {
          if(item.id === route.params?.id ) item.value = route.params?.valor
        }); 
      }
      
      const onSelected = (item) => {
        navigation.navigate("segunda",{
          objeto : item,
        });
      };
      
      const onAdd= () => {
        setModalVisible(!modalVisible);
      };
    
      const addItem = () => {
        if (inputvalue) {
          setItemList([
            ...itemList,
            {
              id: itemList.length + 1,
              value: item
            } 
          ])
          setItem('');
          setModalVisible(!modalVisible);
        }
      }
      
      const onHandlerModal = (id) => {
        setItemSelected(itemList.find(item => item.id === id));
      }

      return (
        <View style={styles.container}>
          <View style={styles.containerGeneral}>
            
          <View style={styles.buttonContainer}>
              <Button title="Agregar Clasificacion" onPress={() => onAdd()} color="#fff" />
          </View>
              
          <Text style={styles.message} >1. Clic en algun elemento para modificar y pasar a la siguiente pantalla"</Text>
          <Text style={styles.message} >2. Puede agregar mas elementos"</Text>
            
            <View style={styles.itemList}>
              <FlatList 
                data={itemList}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
              />
            </View>
         </View>
         <CustomModal animationType='fade' modalVisible={modalVisible}>
            <View style={styles.modal}>
              <View style={styles.modalContent}>
                <View style={styles.modalBox}>
                   <TouchableOpacity
                      onPress={()=>  setModalVisible(!modalVisible)}>
                    <Image source={close} />
                  </TouchableOpacity>
                  
                </View>
                <View>
                  <CustomInput 
                    item={item}
                    onChangeText={onChangeText}
                    placeholder='Escriba categoria'
                    onPressButton={addItem}
                    inputValue={inputvalue}
                    buttonText='Agregar'
                  />
                </View>
              </View>
            </View>
         </CustomModal>
        </View>
      );

};

export default PrimeraVista;