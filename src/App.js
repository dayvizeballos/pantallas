import 'react-native-gesture-handler';

import { Button, FlatList, Image, SafeAreaView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { CustomInput, CustomModal, ListaItems } from './components/index';
import React, { useState } from 'react';

import AppNavigator from './navigation/index';
import AppNavigatorMenu from './navigation-menu/index';
import PrimeraVista from './screens/first/index';
import SegundaVista from './screens/second/index';
import { styles } from "./styles";
import { useFonts } from 'expo-font';

export default function App() {
  const [textCategoria, setTextCategoria] = useState();
  const [loaded] = useFonts({
     'Bebas': require('../assets/fonts/BebasNeue-Regular.ttf'),
  });

  const onSelectItem = (item) => {
    setTextCategoria(item);
  };
  
  if (!loaded) {
    return null;
  }

  
  return (
    <AppNavigator />
    // <AppNavigatorMenu />
  );
}
