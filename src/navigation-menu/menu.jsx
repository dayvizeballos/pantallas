import ContactoVista  from "../screens/contacto/index";
import { NavigationContainer } from "@react-navigation/native";
import PrimeraVista from '../screens/first/index';
import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";

const Menu = createDrawerNavigator();

const MenuNavigator = () => {
    return (
            <Menu.Navigator>
                <Menu.Screen name="primera" component={PrimeraVista}/>
                <Menu.Screen name="contacto" component={ContactoVista}/>
            </Menu.Navigator>
    );
};

export default MenuNavigator;