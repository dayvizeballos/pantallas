import MenuNavigator from './menu';
import { NavigationContainer } from "@react-navigation/native";
import React from "react";

const AppNavigatorMenu = () => {
    return (
        <NavigationContainer>
            <MenuNavigator />
        </NavigationContainer>
    );
};

export default AppNavigatorMenu;
