import { Image, Text, TouchableOpacity, View } from "react-native";

import React from "react";
import { styles } from "./styles";

const listaItems = ({item, onHandlerModal, onSelected}) => {

    const funcionesVarios = (obj) => {
      onSelected(obj)
      onHandlerModal(obj.id);
    }

    return (
    
      <TouchableOpacity style={styles.itemContainer}
                  onPress={()=>{funcionesVarios(item)}}>
          <View>
            <Text style={styles.item}>{item.value}</Text>
          </View>
          <View>
            <Image
              source={require('../../../assets/pantalon.jpg')}
              style={styles.image}
              resizeMode="contain"
            />
          </View>
      </TouchableOpacity>
    
    )
}

export default listaItems;